import React, {Component} from "react";
import Box from "./box/box";


export default class Product extends Component {
productCat = [
    {
        name: 'Face',
        image: 'https://thumbs.dreamstime.com/b/environment-earth-day-hands-trees-growing-seedlings-bokeh-green-background-female-hand-holding-tree-nature-field-gra-130247647.jpg'
    },
    {
        name: 'Body',
        image: 'https://thumbs.dreamstime.com/b/environment-earth-day-hands-trees-growing-seedlings-bokeh-green-background-female-hand-holding-tree-nature-field-gra-130247647.jpg'
    },
    {
        name: 'Hair',
        image: 'https://thumbs.dreamstime.com/b/environment-earth-day-hands-trees-growing-seedlings-bokeh-green-background-female-hand-holding-tree-nature-field-gra-130247647.jpg'
    },
    {
        name: 'Body',
        image: 'https://thumbs.dreamstime.com/b/environment-earth-day-hands-trees-growing-seedlings-bokeh-green-background-female-hand-holding-tree-nature-field-gra-130247647.jpg'
    },
    {
        name: 'Body',
        image: 'https://thumbs.dreamstime.com/b/environment-earth-day-hands-trees-growing-seedlings-bokeh-green-background-female-hand-holding-tree-nature-field-gra-130247647.jpg'
    },
    {
        name: 'Shampoo',
        image: 'https://thumbs.dreamstime.com/b/environment-earth-day-hands-trees-growing-seedlings-bokeh-green-background-female-hand-holding-tree-nature-field-gra-130247647.jpg'
    },
]
    constructor(props) {
        super(props);
        this.state = {
            productCategory: this.productCat
        }
    }

    componentDidMount() {
    }

    render() {
        return (
            <div>
                   <div className='d-flex p-4 mt-5 overflow-auto'>
                           {
                               this.state.productCategory.map((value, index) => {
                                   return  <Box name={value.name} image={value.image}/>
                               })
                           }
                   </div>
                <div className='container'>
                    <div className='row'>
                        <div className='col-4'>
                            <img className='img-fluid' src='https://im4.ezgif.com/tmp/ezgif-4-cc79ca10dfd9.jpg'/>
                        </div>
                        <div className='col-8 mt-2'>
                            <div className='row flex-column'><b>Product Name</b>
                                <small> <i>(300 ml)</i></small>
                            </div>
                            <div className='row'><b>Rs 500</b>
                            </div>
                            <div className='row mt-2'>
                                <div className='add-cart-btn'>Add to Cart</div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div className='container'>
                    <div className='row'>
                        <div className='col-4'>
                            <img className='img-fluid' src='https://im4.ezgif.com/tmp/ezgif-4-cc79ca10dfd9.jpg'/>
                        </div>
                        <div className='col-8 mt-2'>
                            <div className='row flex-column'><b>Product Name</b>
                                <small> <i>(300 ml)</i></small>
                            </div>
                            <div className='row'><b>Rs 500</b>
                            </div>
                            <div className='row mt-2'>
                                <div className='add-cart-btn'>Add to Cart</div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
            </div>
        );
    }

}
