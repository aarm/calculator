import React, {Component} from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import {debugLog} from "next/dist/telemetry/trace/shared";

export default class Calculator extends Component {
      coloumn = { name: '', rate: '', quantity: '', basic_cost: '', discount: '', discount_amt: '' , final_cost: '', taxes: '', tax_amt: '',tool: 'Delete', total_cost: ''}

    constructor(props) {
        super(props);
        this.state = {
            disable: false,
            students: [
                { name: '', rate: '', quantity: '', basic_cost: '', discount: '', discount_amt: '' , final_cost: '', taxes: '', tax_amt: '', tool: 'Delete', total_cost: ''}
            ],
            TotalBasicCost: 0.0,
            TotalDiscount: 0.0,
            TotalFBC: 0.0,
            TotalTax: 0.0,
            FinalPrice: 0.0,
        }
    }

    componentDidMount() {
        this.save()
    }

    calculateBasicCost(e, index, list) {
         const newList = list.map((counter, i) => {
            if (i !== index) {
                return counter
            } else {
                let cost = counter.rate * counter.quantity
                return {...counter, basic_cost: parseFloat(cost)};
            }
        });
        this.setState({students: newList} , () => {
            this.totalBasicCost(); this.save()
        });
        this.calculateDiscountAmt(e, index, newList)
        this.calculateFinalBasicCost(e, index, newList)
        // this.totalBasicCost()
        // this.save()
    }
    calculateDiscountAmt(e, index, list) {
        const newList = list.map((counter, i) => {
            if (i !== index) {
                return counter
            } else {
                let cost = (counter.basic_cost * counter.discount)/100
                return {...counter, discount_amt: parseFloat(cost)};
            }
        });
        this.setState({students: newList}, () => {
            this.totalDiscount(); this.save()
        });
        this.calculateFinalBasicCost(e, index, newList)
        // this.totalDiscount()
    }
    calculateTaxAmt(e, index, list) {
        const newList = list.map((counter, i) => {
            if (i !== index) {
                return counter
            } else {
                let cost = (counter.final_cost * counter.taxes)/100
                return {...counter, tax_amt: parseFloat(cost)};
            }
        });
        this.setState({students: newList} , () => {
            this.save()
        });
        this.calculateTotalCost(e, index, newList)
        // this.save()
    }

    calculateFinalBasicCost(e, index, list) {
        const newList = list.map((counter, i) => {
            if (i !== index) {
                return counter
            } else {
                let cost = counter.basic_cost - counter.discount_amt
                return {...counter, final_cost: parseFloat(cost)};
            }
        });
        this.setState({students: newList} , () => {
            this.totalFBC(); this.save()
        });
        this.calculateTaxAmt(e, index, newList)
        this.calculateTotalCost(e, index, newList)
        // this.totalFBC()
        // this.save()
    }
    calculateTotalCost(e, index, list) {
        const newList = list.map((counter, i) => {
            if (i !== index) {
                return counter
            } else {
                let cost = counter.final_cost + counter.tax_amt
                return {...counter, total_cost: parseFloat(cost)};
            }
        });
        this.setState({students: newList}, () => {
            this.totalFinalCost(); this.save()
        });
        // this.totalFinalCost()
        // this.save()
    }

    handleDiscount(event, index) {
        const newList = this.state.students.map((counter, i) => {
            if (i !== index) return counter;
            return {...counter, discount: parseFloat(event.target.value)};
        });
        this.setState({students: newList}, () => {
            this.totalDiscount(); this.save()
        });
        this.calculateDiscountAmt(event, index, newList)
        // this.totalDiscount()
        // this.save()
    }
    handleTaxes(event, index) {
        const newList = this.state.students.map((counter, i) => {
            if (i !== index) return counter;
            return {...counter, taxes: parseFloat(event.target.value)};
        });
        this.setState({students: newList}, () => {
            this.totalTax(); this.save()
        });
        this.calculateTaxAmt(event, index, newList)
        // this.totalTax()
        // this.save()
    }
    handleRate(event, index) {
        const newList = this.state.students.map((counter, i) => {
            if (i !== index) return counter;
            return {...counter, rate: parseFloat(event.target.value)};
        });
        this.setState({students: newList}, () => {
            this.totalBasicCost(); this.save()
        });
        this.calculateBasicCost(event, index, newList)
        // this.save()
    }

    handleQuantity(event, index) {
        const newList = this.state.students.map((counter, i) => {
            if (i !== index) return counter;
            return {...counter, quantity: parseFloat(event.target.value)};
        });
        this.setState({students: newList}, () => {
            this.totalBasicCost(); this.save()
        });
        this.calculateBasicCost(event, index, newList)
    }
    handleName(event, index) {
        const newList = this.state.students.map((counter, i) => {
            if (i !== index) return counter;
            return {...counter, name: event.target.value};
        });
        this.setState({students: newList}, () => {
            this.save()
        });
    }

    addNewItem() {
          let list = this.state.students
        list.push(this.coloumn)
        this.setState({list}, () => {
            this.save()
        })
    }

    deleteItem(event, index) {
        let list = this.state.students
        list.splice(index, 1)
        this.setState({list}, () => {
            this.save(); this.totalDiscount(); this.totalFBC(); this.totalFinalCost(); this.totalTax(); this.totalBasicCost()
        })
    }

    totalBasicCost() {
          let total = 0;
          this.state.students.forEach((basic, index) => {
              total = basic.basic_cost + parseFloat(total)
          })
        this.setState({TotalBasicCost: total})
    }
    totalDiscount() {
        let total = 0;
        this.state.students.forEach((basic, index) => {
            total = basic.discount + parseFloat(total)
        })
        this.setState({TotalDiscount: total})
    }
    totalFBC() {
        let total = 0;
        this.state.students.forEach((basic, index) => {
            total = basic.final_cost + parseFloat(total)
        })
        this.setState({TotalFBC: total}, () => {
            this.save()
        })
    }
    totalTax() {
        let total = 0;
        this.state.students.forEach((basic, index) => {
            total = basic.taxes + parseFloat(total)
        })
        this.setState({TotalTax: total}, () => {
            this.save()
        })
    }
    totalFinalCost() {
        let total = 0;
        this.state.students.forEach((basic, index) => {
            total = basic.total_cost + parseFloat(total)
        })
        this.setState({FinalPrice: total}, () => {
            this.save()
        })
    }

    save() {
        for (let i =0 ; i < this.state.students.length; i++) {
            for (const property in this.state.students[i]) {
                if (this.state.students[i][property] === '' || Number.isNaN(this.state.students[i][property]) ) {
                    this.setState({disable: true})
                    break
                } else {
                    this.setState({disable: false})
                }
            }
        }
    }

    setInterver() {
        setInterval(() => {this.delete()} , 5000);
    }

    delete() {
        let newList =  this.state.students.splice(0, this.state.students.length-1)
        this.setState({students: newList})
    }

    finalResult() {
       let data = {
           data : {
               all_data: this.state.students,
               total_basic_cost: this.state.TotalBasicCost,
               total_tax: this.state.TotalTax,
               total_final_basic_cost: this.state.TotalFBC,
               total_discount: this.state.TotalDiscount,
               total_final_cost: this.state.FinalPrice,
           },
           message: 'success'
       }
       console.log(data)
        alert(JSON.stringify(data))
    }

    render() {
        return(
            <div className='container card p-4' >
                <div className='row float-right'>
                    <div className='col-md-12 col-sm-12'>
                        <div className='btn btn-primary float-right' onClick={this.addNewItem.bind(this)}>Add New Item</div>
                    </div>
                    <div className='col-md-12 col-sm-12'>
                        <div className='btn btn-primary float-right' onClick={this.setInterver.bind(this)}>Delete All</div>
                    </div>
                </div>
                <div className='row mt-5'>
                    <div className='col-md-12'>
                        <div className="table-responsive">
                            <div className="row bg-light border rounded m-2 p-2 border-dark">
                                <div className="col-1 col-md-1 h6 ">Id</div>
                                <div className="col-1 col-md-1 h6 ">Name</div>
                                <div className="col-1 col-md-1  h6">Rate</div>
                                <div className="col-1 col-md-1  h6">Quantity</div>
                                <div className="col-1 col-md-1 h6">Basic Cost</div>
                                <div className="col-1 col-md-1 h6">Discount (%)</div>
                                <div className="col-1 col-md-1 h6">Discount Amt</div>
                                <div className="col-1 col-md-1 h6">Final Basic Cost</div>
                                <div className="col-1 col-md-1 h6">Taxes (%)</div>
                                <div className="col-1 col-md-1 h6">Tax Amt</div>
                                <div className="col-1 col-md-1 h6">Total cost</div>
                                <div className="col-1 col-md-1 h6">Tool</div>
                            </div>
                            {
                                this.state.students.map((role, index) => (
                                    <div className="row border-bottom  m-2 p-2 ">
                                        <div className="col-1 col-md-1 text-truncate ">{index + 1}</div>
                                        <div className="col-1 col-md-1 text-truncate "><input className='w-100' onChange={e => (this.handleName(e, index))} value={role.name}/></div>
                                        <div className="col-1 col-md-1 text-truncate "><input className='w-100' onChange={e => (this.handleRate(e, index))} type='number' value={role.rate}/></div>
                                        <div className="col-1 col-md-1  text-truncate"><input className='w-100' onChange={e => (this.handleQuantity(e, index))} type='number' value={role.quantity}/></div>
                                        <div className="col-1 col-md-1  text-truncate"><input className='w-100' disabled type='number' value={role.basic_cost}/></div>
                                        <div className="col-1 col-md-1  text-truncate"><input className='w-100' onChange={e => (this.handleDiscount(e, index))} type='number' value={role.discount}/></div>
                                        <div className="col-1 col-md-1  text-truncate"><input className='w-100' type='number' disabled value={role.discount_amt}/></div>
                                        <div className="col-1 col-md-1  text-truncate"><input className='w-100' type='number' disabled value={role.final_cost}/></div>
                                        <div className="col-1 col-md-1  text-truncate"><input className='w-100' onChange={e => (this.handleTaxes(e, index))} type='number' value={role.taxes}/></div>
                                        <div className="col-1 col-md-1  text-truncate"><input className='w-100' type='number' disabled value={role.tax_amt}/></div>
                                        <div className="col-1 col-md-1  text-truncate"><input className='w-100' type='number' disabled value={role.total_cost}/></div>
                                        <div className="col-1 col-md-1  text-truncate"><button className='btn btn-outline-primary' onClick={event => this.deleteItem(event, index)}>{role.tool}</button></div>
                                        <div className="col-2 col-md-6  ">
                                            <div className="float-right">

                                            </div>
                                        </div>
                                    </div>
                                    )
                                )
                            }
                        </div>
                    </div>
                </div>
                <div className='row mt-4'>
                    <div className='col-md-6'></div>
                    <div className='col-md-4 col-sm-12 d-flex justify-content-center'>
                        <table className='w-100'>
                            <tr>
                                <td>Total Basic Cost</td>
                                <td>{this.state.TotalBasicCost}</td>
                            </tr>
                            <tr>
                                <td>Total Discount</td>
                                <td>{this.state.TotalDiscount}</td>
                            </tr>
                            <tr>
                                <td>Total Final Basic Cost</td>
                                <td>{this.state.TotalFBC}</td>
                            </tr>
                            <tr>
                                <td>Total Tax</td>
                                <td>{this.state.TotalTax}</td>
                            </tr>
                            <tr>
                                <td>Final Price</td>
                                <td>{this.state.FinalPrice}</td>
                            </tr>
                        </table>
                </div>
                    <div className='col-md-2'></div>
                </div>
                <div className='row float-right'>
                    <div className='col-md-12 col-sm-12'>
                        <button className='btn btn-primary float-right' disabled={this.state.disable} onClick={this.finalResult.bind(this)}>Save</button>
                    </div>
                </div>
            </div>
        )
    }
}

