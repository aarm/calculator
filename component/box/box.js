import React, {Component} from "react";


export default class Box extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
    }

    render() {
        return (
            <div>
               <div className='card  mr-3' style={{backgroundImage: "url(" + `${this.props.image}` + ")"}}>
                   <div className='card-body'>
                       <div className='product-name'  >{this.props.name}</div>
                   </div>
               </div>
            </div>
        );
    }

}
